
package br.com.senac.ex10;

public class ControleEstoque {

    Estoque estoque = new Estoque();

    public void controleEstoque() {

        double minimo = 125;

        if (estoque.margemEstoque() < minimo) {

            System.out.println("O estoque esta muito baixo");

        }
        System.out.println("O estoque esta dentro do permitido");
    }
//
//    Cada classe cuidando de uma unica responsabilidade o que deixa o sistema coeso...
//    
//    e com baixo aclopamento as classe e objetos so relacionam-se dentro do necessario...
//    
}
